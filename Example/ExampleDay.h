//
//  ExampleDay.h
//  Example
//
//  Created by Kristopher Conrad on 5/3/15.
//  Copyright (c) 2015 Funk-iSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExampleDay : NSObject

@property (nonatomic, retain) NSString  *month,
                                        *weekday,
                                        *date;
@property (nonatomic, retain) NSArray   *events;

@end
