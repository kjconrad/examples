//
//  HSView.h
//  Examples
//
//  Created by Kristopher Conrad on 5/2/15.
//  Copyright (c) 2015 Funk-iSoft. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "HSCellView.h"
@class HSView;
@class HSCellView;

@protocol HSViewDataSource <NSObject>
@required
-(int)numberOfSectionsInHSView: (HSView *) hsView;
-(int)numberOfCellsInSection: (int) section forHSView: (HSView *) hsView;
-(void)hsview:(HSView *) hsView didSelectCellAtIndexPath: (NSIndexPath *) indexPath;
-(HSCellView *)hsview: (HSView *) hsView cellForIndexPath: (NSIndexPath *) indexPath;
-(void)hidePointer: (BOOL) hide animated: (BOOL) animated;
@end


@interface HSView : UIScrollView <UIScrollViewDelegate>
{
    int             lastX,
                    liveCellCount,
                    cellCount;
    
    BOOL            scrollingLeft,
                    itWasShrunk;
    
    UIView          *dividerBar;
}

@property (nonatomic, assign) int cellWidth;
@property (nonatomic, retain) NSIndexPath *startingIndexPath;
@property (nonatomic, assign) id<HSViewDataSource> dataSource;
@property (nonatomic, retain) NSMutableArray    *unusedCells,
                                                *usedCells;
@property (nonatomic, retain) HSCellView      *enlargedCell;


-(void)reloadData;
-(void)scrollToIndexPath: (NSIndexPath *) indexPath animated: (BOOL) animated;
-(HSCellView *)unusedCell;

@end
