//
//  HSCellView.m
//  Examples
//
//  Created by Kristopher Conrad on 5/2/15.
//  Copyright (c) 2015 Funk-iSoft. All rights reserved.
//

#import "HSCellView.h"
#import <QuartzCore/QuartzCore.h>

@implementation HSCellView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.autoresizesSubviews = YES;
        
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(2,
                                                                  0,
                                                                  frame.size.width - 4,
                                                                  frame.size.height - 4)];
        bgView.layer.cornerRadius = 4;
        bgView.clipsToBounds = YES;
        bgView.backgroundColor = [UIColor redColor];
        bgView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self addSubview:bgView];
        
        UIView *whiteView = [[UIView alloc] initWithFrame:CGRectMake(6,
                                                                     28,
                                                                     bgView.frame.size.width - 8,
                                                                     bgView.frame.size.height - 31)];
        whiteView.backgroundColor = [UIColor colorWithWhite:0.8 alpha:1];
        whiteView.layer.cornerRadius = 3;
        whiteView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        whiteView.tag = 37;
        [self addSubview:whiteView];
        
        month = [[UILabel alloc] initWithFrame:CGRectMake(7,
                                                          5,
                                                          frame.size.width - 14,
                                                          20)];
        month.textAlignment = NSTextAlignmentCenter;
        month.font = [UIFont boldSystemFontOfSize:16];
        month.textColor = [UIColor whiteColor];
        month.backgroundColor = [UIColor clearColor];
        month.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:month];
        
        dayLabel = [[UILabel alloc] initWithFrame:CGRectMake(7,
                                                             month.frame.origin.y + 28,
                                                             frame.size.width - 14,
                                                             18)];
        dayLabel.backgroundColor = [UIColor clearColor];
        dayLabel.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
        dayLabel.textAlignment = NSTextAlignmentCenter;
        dayLabel.textColor = [UIColor redColor];
        dayLabel.font = [UIFont boldSystemFontOfSize:15];
        [self addSubview:dayLabel];
        
        day = [[UILabel alloc] initWithFrame:CGRectMake(7,
                                                        dayLabel.frame.origin.y + 15,
                                                        frame.size.width - 14,
                                                        frame.size.height - dayLabel.frame.origin.y - 28)];
        day.textAlignment = NSTextAlignmentCenter;
        day.backgroundColor = [UIColor clearColor];
        day.textColor = [UIColor colorWithWhite:0.1 alpha:1];
        day.font = [UIFont boldSystemFontOfSize:day.frame.size.height - 6];
        day.adjustsFontSizeToFitWidth = YES;
        day.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight;
        [self addSubview:day];
        
    }
    return self;
}

-(void) setMonth: (NSString *) string
{
    month.text = string;
}

-(void) setDay: (NSString *) string
{
    day.text = string;
}

-(void) setDayName: (NSString *) string
{
    dayLabel.text = string;
}

@end
