//
//  ViewController.h
//  Example
//
//  Created by Kristopher Conrad on 5/2/15.
//  Copyright (c) 2015 Funk-iSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HSView.h"

@interface ViewController : UIViewController <HSViewDataSource, UITableViewDataSource>
{
    HSView          *topSlider;
    UIView          *pointer;
    UITableView     *mainTableView;
}

@property (nonatomic, retain) NSMutableArray *months;
@property (nonatomic, retain) NSIndexPath *currentSelection;

@end

