//
//  ViewController.m
//  Example
//
//  Created by Kristopher Conrad on 5/2/15.
//  Copyright (c) 2015 Funk-iSoft. All rights reserved.
//

#import "ViewController.h"
#import "ExampleDay.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.months = [NSMutableArray array];
    
    self.view.backgroundColor = [UIColor whiteColor];
    topSlider = [[HSView alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, 135)];
    topSlider.dataSource = self;
    [self.view addSubview:topSlider];
    
    UIBezierPath* trianglePath = [UIBezierPath bezierPath];
    [trianglePath moveToPoint:CGPointMake(0, 0)];
    [trianglePath addLineToPoint:CGPointMake(30,0)];
    [trianglePath addLineToPoint:CGPointMake(15, 20)];
    [trianglePath closePath];
    
    CAShapeLayer *triangleMaskLayer = [CAShapeLayer layer];
    [triangleMaskLayer setPath:trianglePath.CGPath];
    
    mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,
                                                             155,
                                                             self.view.frame.size.width,
                                                             self.view.frame.size.height - 155)
                                             style:UITableViewStylePlain];
    mainTableView.dataSource = self;
    [self.view addSubview:mainTableView];
    
    pointer = [[UIView alloc] initWithFrame:CGRectMake((self.view.frame.size.width - 30) / 2,
                                                            topSlider.frame.origin.y + topSlider.frame.size.height - 16,
                                                            36,
                                                            20)];
    
    pointer.backgroundColor = [UIColor redColor];
    pointer.layer.mask = triangleMaskLayer;
    [self.view addSubview:pointer];
    
    [self parseSampleData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)parseSampleData
{
    NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"example" ofType:@"json"];
    NSData *jsonData = [NSData dataWithContentsOfFile:jsonPath];
    
    NSError *jsonError;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&jsonError];
    if (jsonError) {
        NSLog(@"***Error*** Failed to read json file");
        return;
    }
    
    for (NSDictionary *monthDict in jsonArray) {
        NSMutableArray *aMonth = [NSMutableArray array];
        NSString *monthName = monthDict[@"month"];
        NSArray *daysArray = monthDict[@"days"];
        
        for (NSDictionary *dayDict in daysArray) {
            ExampleDay *aDay = [[ExampleDay alloc] init];
            aDay.month = monthName;
            aDay.date = dayDict[@"date"];
            aDay.weekday = dayDict[@"weekday"];
            aDay.events = dayDict[@"events"];
            
            [aMonth addObject:aDay];
        }
        
        [self.months addObject:aMonth];
    }
    [topSlider reloadData];
}

#pragma mark - Table view data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *days = self.months[self.currentSelection.section];
    ExampleDay *thisDay = days[self.currentSelection.item];
    return thisDay.events.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    NSArray *days = self.months[self.currentSelection.section];
    ExampleDay *thisDay = days[self.currentSelection.item];
    
    cell.textLabel.text = thisDay.events[indexPath.item];
    return cell;
}

#pragma mark - HSView data source

-(int)numberOfSectionsInHSView: (HSView *) hsView
{
    return (int)self.months.count;
}

-(int)numberOfCellsInSection: (int) section forHSView: (HSView *) hsView
{
    NSArray *days = self.months[section];
    return (int)days.count;
}

-(void)hsview:(HSView *) hsView didSelectCellAtIndexPath: (NSIndexPath *) indexPath
{
    self.currentSelection = indexPath;
    [mainTableView reloadData];
}

-(HSCellView *)hsview: (HSView *) hsView cellForIndexPath: (NSIndexPath *) indexPath
{
    HSCellView *cell = [hsView unusedCell];
    
    NSArray *days = self.months[indexPath.section];
    ExampleDay *thisDay = days[indexPath.item];
    
    [cell setMonth:thisDay.month];
    [cell setDay:thisDay.date];
    [cell setDayName:thisDay.weekday];
    
    return cell;
}

-(void)hidePointer: (BOOL) hide animated: (BOOL) animated
{
    CGRect frame = pointer.frame;
    frame.size.height = hide ? 0 : 20;
        
    if (animated) {
        [UIView animateWithDuration:0.1 animations:^{
            pointer.frame = frame;
        }];
    }
    else {
        pointer.frame = frame;
    }
}


@end
