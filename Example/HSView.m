//
//  HSView.m
//  Examples
//
//  Created by Kristopher Conrad on 5/2/15.
//  Copyright (c) 2015 Funk-iSoft. All rights reserved.
//


#import "HSView.h"

@implementation HSView

-(void)dealloc
{
    for (UIView *view in self.usedCells) {
        [view removeFromSuperview];
    }
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = false;
        self.backgroundColor = [UIColor darkGrayColor];
        self.showsHorizontalScrollIndicator = NO;
        self.cellWidth = 76;
        self.unusedCells = [NSMutableArray array];
        self.usedCells = [NSMutableArray array];
        self.bounces = NO;
        self.delegate = self;
        lastX = 0;
        self.startingIndexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        
        dividerBar = [[UIView alloc] initWithFrame:CGRectMake(0,
                                                              frame.size.height - 5,
                                                              self.frame.size.width * 2,
                                                              5)];
        dividerBar.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:dividerBar];
        
        
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapView:)];
        [self addGestureRecognizer:tap];
        
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longTouch:)];
        [self addGestureRecognizer:longPress];
    }
    return self;
}

-(void)reloadData
{
    if (self.enlargedCell) {
        [self shrinkEnlargedCellAnimated:NO];
    }
    
    //clear out any instantiated cells
    for (UIView *view in self.usedCells) {
        [view removeFromSuperview];
    }
    [self.usedCells removeAllObjects];
    [self.unusedCells removeAllObjects];
    
    cellCount = 0;
    if (self.dataSource) {
        for (int i = 0; i < [self.dataSource numberOfSectionsInHSView:self]; ++i) {
            cellCount += [self.dataSource numberOfCellsInSection:i forHSView:self];
        }
    }
    
    if (cellCount == 0) {
        NSLog(@"no cells");
        [self.dataSource hidePointer:YES animated:NO];
        return;
    }
    
    //Calculating the content size for scroll view
    //must add addiional padding so the first and last cell can scroll to the center of the view
    self.contentSize = CGSizeMake(((cellCount - 1) * (self.cellWidth + 10)) + self.frame.size.width, self.frame.size.height);
    dividerBar.frame = CGRectMake(dividerBar.frame.origin.x,
                                  dividerBar.frame.origin.y,
                                  self.contentSize.width < 400 ? 400 : self.contentSize.width,
                                  5);
    [self scrollToIndexPath:self.startingIndexPath animated:NO];

    [self addInitialCells];
    
    [self enlargeCenterCellAnimated:NO];
}

-(void)addInitialCells
{
    //Load twice as many cells as will fit in the view, or all cells, which ever is smaller
    int cellsToLoad = (self.frame.size.width / self.cellWidth) * 2;
    cellsToLoad = cellsToLoad > cellCount ? cellCount : cellsToLoad;
    int staringX = self.contentOffset.x - (self.frame.size.width / 2);
    NSIndexPath *cellIndexPath = [self indexPathForXPoint:staringX];
    if (!cellIndexPath) {
        cellIndexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    }
    while (cellIndexPath != nil && self.usedCells.count < cellsToLoad) {
        HSCellView *cell = [self.dataSource hsview:self cellForIndexPath:cellIndexPath];
        cell.frame = [self frameForCellAtIndexPath:cellIndexPath];
        cell.indexPath = cellIndexPath;
        
        [self addSubview:cell];
        [self.usedCells addObject:cell];
        
        cellIndexPath = [self indexPathAfter:cellIndexPath];
    }
}

-(HSCellView *)unusedCell
{
    HSCellView *cell;
    if (self.unusedCells.count > 0) {
        cell = [self.unusedCells firstObject];
        [self.unusedCells removeObject:cell];
    }
    else {
        cell = [[HSCellView alloc] initWithFrame:[self frameForCellAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]]];
    }
    
    return  cell;
}

#pragma mark - touch gestures

-(void)didTapView: (UITapGestureRecognizer *) tap
{
    if ([self.enlargedCell.indexPath isEqual: [self indexPathForXPoint:[tap locationInView:self].x]]) {
        return;
    }
    
    CGFloat tapX = [tap locationInView:self.superview].x;
    
    CGFloat contentX = self.contentOffset.x;
    contentX += (self.frame.size.width / 2);
    NSIndexPath *centerIndexPath = [self indexPathForXPoint:contentX];
    
    if (tapX > self.frame.size.width / 2) { // scroll forward
        if (tapX > self.frame.size.width - 45) { //next section
            NSIndexPath *moveToIndexPath;
            int lastSection = [self.dataSource numberOfSectionsInHSView:self] - 1;
            if (centerIndexPath.section < lastSection) {
                moveToIndexPath = [NSIndexPath indexPathForItem:0 inSection:centerIndexPath.section + 1];
            }
            else {
                NSIndexPath *lastIndex = [NSIndexPath indexPathForItem:[self.dataSource numberOfCellsInSection:lastSection forHSView:self] - 1 inSection:lastSection];
                if (![centerIndexPath isEqual:lastIndex]) {
                    moveToIndexPath = lastIndex;
                }
            }
            
            if (moveToIndexPath) {
                self.userInteractionEnabled = NO;
                [self scrollToIndexPath:moveToIndexPath animated:YES];
            }
        }
        else { // next cell
            NSIndexPath *nextCell = [self indexPathAfter:self.enlargedCell.indexPath];
            if (nextCell) {
                self.userInteractionEnabled = NO;
                [self scrollToIndexPath:nextCell animated:YES];
            }
        }
    }
    else { // scroll backwards
        if (tapX < 45) { //previous section
            NSIndexPath *moveToIndexPath;
            if (centerIndexPath.section > 0) {
                int newSection = centerIndexPath.item == 0 ? (int)centerIndexPath.section - 1 : (int)centerIndexPath.section;
                moveToIndexPath = [NSIndexPath indexPathForItem:0 inSection:newSection];
            }
            else {
                NSIndexPath *firstIndex = [NSIndexPath indexPathForItem:0 inSection:0];
                if (![centerIndexPath isEqual:firstIndex]) {
                    moveToIndexPath = firstIndex;
                }
            }
            
            if (moveToIndexPath) {
                self.userInteractionEnabled = NO;
                [self scrollToIndexPath:moveToIndexPath animated:YES];
            }
        }
        else { // previous cell
            NSIndexPath *prevCell = [self indexPathBefore:self.enlargedCell.indexPath];
            if (prevCell) {
                self.userInteractionEnabled = NO;
                [self scrollToIndexPath:prevCell animated:YES];
            }
        }
    }
}

-(void)longTouch: (UILongPressGestureRecognizer *) longPress
{
    if (longPress.state == UIGestureRecognizerStateBegan) {
        if (self.enlargedCell.indexPath != self.startingIndexPath) {
            [self scrollToIndexPath:self.startingIndexPath animated:YES];
        }
    }
}

#pragma mark - index calculations

-(CGRect) frameForCellAtIndexPath: (NSIndexPath *) indexPath
{
    CGRect frame = CGRectMake([self xPointForCellAtIndexPath:indexPath],
                              15,
                              self.cellWidth,
                              self.frame.size.height - 33);
    return frame;
}

-(int) xPointForCellAtIndexPath: (NSIndexPath *) indexPath
{
    int x = (self.frame.size.width - self.cellWidth) / 2;
    
    for (int i = 0; i < indexPath.section; ++i) {
        x += [self.dataSource numberOfCellsInSection:i forHSView:self] * (self.cellWidth + 10);
    }
    
    x += indexPath.item * (self.cellWidth + 10);
    
    return  x;
}

-(NSIndexPath *) indexPathForXPoint: (CGFloat) x
{
    int section = -1;
    int item;
    
    int noOfSections = [self.dataSource numberOfSectionsInHSView:self];
    int noOfItems = [self.dataSource numberOfCellsInSection:noOfSections - 1 forHSView:self];
    NSIndexPath *lastItem = [NSIndexPath indexPathForItem:noOfItems inSection:noOfSections];
    
    if (x >= [self xPointForCellAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]] &&
        x <= [self xPointForCellAtIndexPath:lastItem]) {
        //verifying x point doesn't fall in our padding
        for (int thisSection = 1; thisSection < noOfSections; ++thisSection) {
            if (x < [self xPointForCellAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:thisSection]]) {
                section = thisSection - 1;
                break;
            }
            else {
                if (thisSection == noOfSections - 1) {
                    section = thisSection;
                    break;
                }
            }
        }
        if (section > -1) {
            int whatsLeft = x - [self xPointForCellAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:section]];
            item = whatsLeft / (self.cellWidth + 10);
            
            return [NSIndexPath indexPathForItem:item inSection:section];
        }
    }
    
    return nil;
}

-(NSIndexPath *) indexPathBefore: (NSIndexPath *) indexPath
{
    int section = (int)indexPath.section;
    int item = (int)indexPath.item;
    
    if (item > 0) {
        --item;
    }
    else {
        if (section == 0) {
            return nil;
        }
        --section;
        item = [self.dataSource numberOfCellsInSection:section forHSView:self] - 1;
    }
    NSIndexPath *newIndexPath = [NSIndexPath indexPathForItem:item inSection:section];
    return newIndexPath;
}

-(NSIndexPath *) indexPathAfter: (NSIndexPath *) indexPath
{
    int section = (int)indexPath.section;
    int item = (int)indexPath.item + 1;
    
    if (item >= [self.dataSource numberOfCellsInSection:section forHSView:self]) {
        ++section;
        item = 0;
    }
    
    if (section < [self.dataSource numberOfSectionsInHSView:self]) {
        return [NSIndexPath indexPathForItem:item inSection:section];
    }
    else {
        return nil;
    }
}

#pragma mark - UIScrollViewDelegate Methods

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (self.enlargedCell) {
        [self shrinkEnlargedCellAnimated:YES];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (lastX < scrollView.contentOffset.x) {
        scrollingLeft = YES;
    }
    else {
        scrollingLeft = NO;
    }
    lastX = scrollView.contentOffset.x;
    
    if (self.usedCells.count == 0) {
        [self addInitialCells];
        return;
    }
    int leftThreshHold = scrollView.contentOffset.x - ((self.cellWidth + 20) * 3);
    int rightThreshHold = scrollView.contentOffset.x + self.frame.size.width + ((self.cellWidth + 20) * 3);
    
    if (scrollingLeft) {
        //remove cells outside the threshold and store them for reuse later
        for (int i = 0; i < self.usedCells.count;) {
            HSCellView *cell = [self.usedCells objectAtIndex:i];
            if (cell.frame.origin.x < leftThreshHold) {
               // DLog(@"removing cell at index %i", cell.index);
                HSCellView *cellToMove = cell;
                [self.unusedCells addObject:cellToMove];
                [self.usedCells removeObject:cell];
                [cellToMove removeFromSuperview];
            }
            else {
                ++i;
            }
        }
        //add unused cell to other side of the scroll view
        HSCellView *lastCell = [self.usedCells lastObject];
        NSIndexPath *nextCell = [self indexPathAfter:lastCell.indexPath];
        if (nextCell) {
            HSCellView *cell = [self.dataSource hsview:self cellForIndexPath:nextCell];
            cell.frame = [self frameForCellAtIndexPath:nextCell];
            cell.indexPath = nextCell;
            
            [self addSubview:cell];
            [self.usedCells addObject:cell];
        }
    }
    else { //Scrolling right
        //remove cells outside the threshold and store them for reuse later
        for (int i = 0; i < self.usedCells.count;) {
            HSCellView *cell = [self.usedCells objectAtIndex:i];
            if (cell.frame.origin.x > rightThreshHold) {
              //  DLog(@"removing cell at index %i", cell.index);
                HSCellView *cellToMove = cell;
                [self.unusedCells addObject:cellToMove];
                [self.usedCells removeObject:cell];
                [cellToMove removeFromSuperview];
            }
            else {
                ++i;
            }
        }
        
        //add unused cell to other side of the scroll view
        HSCellView *firstCell = [self.usedCells firstObject];
        NSIndexPath *prevCell = [self indexPathBefore:firstCell.indexPath];
        if (prevCell) {
            HSCellView *cell = [self.dataSource hsview:self cellForIndexPath:prevCell];
            cell.frame = [self frameForCellAtIndexPath:prevCell];
            cell.indexPath = prevCell;
            
            [self addSubview:cell];
            [self.usedCells insertObject:cell atIndex:0];
        }
    }
}


-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                    withVelocity:(CGPoint)velocity
             targetContentOffset:(inout CGPoint *)targetContentOffset
{
    int targetX = targetContentOffset->x;
    int remainder = targetX % (self.cellWidth + 10);
    int targetIndex = targetX / (self.cellWidth + 10);
    CGPoint newTarget;
    if (remainder > (self.cellWidth / 2)) {
        newTarget = CGPointMake((targetIndex + 1) * (self.cellWidth + 10), targetContentOffset->y);
    }
    else {
        newTarget = CGPointMake((targetIndex) * (self.cellWidth + 10), targetContentOffset->y);
    }
    
    *targetContentOffset = newTarget;
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate) {
        [self enlargeCenterCellAnimated:YES];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self enlargeCenterCellAnimated:YES];
    self.userInteractionEnabled = YES;
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [self enlargeCenterCellAnimated:YES];
    self.userInteractionEnabled = YES;
}

-(void)scrollToIndexPath: (NSIndexPath *) indexPath animated: (BOOL) animated
{
    if (self.enlargedCell) {
        [self shrinkEnlargedCellAnimated:YES];
    }
    //Start with frame from chosen cell, then calculate a view sized frame that centers that cell
    CGRect frame = [self frameForCellAtIndexPath:indexPath];
    frame.origin.x -= (self.frame.size.width - self.cellWidth) / 2;
    frame.origin.y = 0;
    frame.size.width = self.frame.size.width;
    frame.size.height = self.frame.size.height;
    
    [self scrollRectToVisible:frame animated:animated];
}

#pragma mark - Cell Animations

-(void)enlargeCenterCellAnimated: (BOOL) animated
{
    int centerX = self.contentOffset.x + (self.frame.size.width / 2);
    NSIndexPath *centerIndexPath = [self indexPathForXPoint:centerX];
    for (HSCellView *cell in self.usedCells) {
        if ([cell.indexPath isEqual:centerIndexPath]) {
            self.enlargedCell = cell;
        }
    }
    
    if (animated) {
        [UIView animateWithDuration:0.1 animations:^{
            [self enlargeCell];
        } completion:^(BOOL finished) {
            [self doneEnlargingCenterCell];
        }];
    }
    else {
        [self enlargeCell];
        [self doneEnlargingCenterCell];
    }
    
    [self.dataSource hsview:self didSelectCellAtIndexPath:self.enlargedCell.indexPath];
}

-(void) enlargeCell
{
    if (self.enlargedCell) {
        CGRect newSize = CGRectMake(self.enlargedCell.frame.origin.x - 5,
                                    9,
                                    self.cellWidth + 10,
                                    self.frame.size.height - 20);
        CGPoint center = self.enlargedCell.center;
        self.enlargedCell.frame = newSize;
        self.enlargedCell.center = center;
        [self.enlargedCell viewWithTag:37].backgroundColor = [UIColor whiteColor];

    }
}

-(void) shrinkEnlargedCellAnimated: (BOOL) animated
{
    [self.dataSource hidePointer:YES animated:NO];
    if (animated) {
        [UIView animateWithDuration:0.1
                         animations:^{
                             [self shrinkEnlargedCell];
                         }];
    }
    else {
        [self shrinkEnlargedCell];
    }
}

-(void) shrinkEnlargedCell
{
    //Don't call this method directly without calling [self doneShrinkingCell];
    if (self.enlargedCell) {
        self.enlargedCell.frame = [self frameForCellAtIndexPath:self.enlargedCell.indexPath];
        [self.enlargedCell viewWithTag:37].backgroundColor = [UIColor colorWithWhite:.8 alpha:1];
        self.enlargedCell = nil;
    }
}

-(void)doneEnlargingCenterCell
{
    [self.dataSource hidePointer:NO animated:YES];
}


@end
