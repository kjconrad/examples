//
//  HSCellView.h
//  Examples
//
//  Created by Kristopher Conrad on 5/2/15.
//  Copyright (c) 2015 Funk-iSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
@class GameWeek;

@interface HSCellView : UIView
{
    UILabel     *month, *day, *dayLabel;
}

@property (nonatomic, retain) NSIndexPath *indexPath;

-(void) setMonth: (NSString *) string;
-(void) setDay: (NSString *) string;
-(void) setDayName: (NSString *) string;

@end
