//
//  AppDelegate.h
//  Example
//
//  Created by Kristopher Conrad on 5/2/15.
//  Copyright (c) 2015 Funk-iSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

